#!/bin/bash


git config --global user.email "contactliuhao@gmail.com"
git config --global user.name "Hao Liu"
git checkout master
sed -i '$!N;$s/.*\n/'"China\/Shanghai $(TZ='Asia/Shanghai' date '+%Y-%m-%d %H:%M:%S.%3N')"'\n/;P;D' README.md
now=$(TZ='Asia/Shanghai' date '+%Y-%m-%d %H:%M:%S')
git add -A
git commit -m "Auto Push via GitHub Actions at China/Shanghai: $now"
git push "https://CI_PUSH_TOKEN_NAME:$CI_PUSH_TOKEN_VALUE@gitlab.com/beliuhao/beliuhao.git" master